/*
 * shell_a1.c
 *
 * Author: Abdul Haseeb Jehangir
 * Date: September 26, 2014
 * ECSE 427, FALL 2014
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include "arraylist.h"

/*
 * ========================================================================
 * MACROS
 * ========================================================================
 */
#define MAX_LINE 80 /* 80 chars per line, per command, should be enough. */
#define BUFFER 10

/*
 * ========================================================================
 * Data Structures
 * ========================================================================
 */
typedef struct process {
	pid_t id;
	char* cmd;
} process;

/*
 * ========================================================================
 * Function declarations
 * ========================================================================
 */
void process_init(process* pss, pid_t pid, char* command);
void get_running_process();
char* get_cmd(char first_char);
void print_history();
void abs_path();
void ch_dir(char* path);
void foreground(int i);
int setup(char inputBuffer[], char *args[], int *background);

/*
 * ========================================================================
 * Global Variables
 * ========================================================================
 */
// saves list of user entered commands in a dynamic array.
arraylist history;
//keep record of currently running processes in background.
arraylist bg_processes;
// saves user entered command temporarily until stored in history arraylist
char* tmp;

/*
 * ========================================================================
 * Program begins here
 * This program offers an interactive UNIX shell to the user.
 * Functionality is limited as per the requirement of ECSE427 Assignment 1
 * ========================================================================
 */
int main(void)
{
  char inputBuffer[MAX_LINE]; /* buffer to hold the command entered */
  int background,	/* equals 1 if a command is followed by '&' */
      status,
      w;
  char *args[MAX_LINE+1]; /* command line (of 80) has a maximum of 80 characters */
  pid_t child_pid;

  arraylist_init(&history);
  arraylist_init(&bg_processes);
  process* new_process;

  while(1) {
    background = 0;
    printf("COMMAND->\n");
    status = setup(inputBuffer, args, &background);      /* get next command */
    /* the steps are:
    (1) fork a child process using fork()
    (2) the child process will invoke execvp()
    (3) if background == 1, the parent will wait,
    otherwise returns to the setup() function.
    */
	if (status == 0) {
    	child_pid = fork();
		if (child_pid == 0) {
		  execvp(args[0], &args[0]);
		} else if (child_pid == -1) {
			printf("unable to create a child process. Try again.\n");
		} else {
		  // check if ampercent was used at the end of the command.
			if (background != 1) {
			  w = waitpid(child_pid, &status, WUNTRACED | WCONTINUED);

			  if (w == -1) {
			  		error("Unable to wait for a program");
			  }
			} else {
				new_process = malloc(sizeof(process));
				process_init(new_process, child_pid, args[0]);
				arraylist_add(&bg_processes, (void*)new_process);
			}

		}
  	}
  }
  return 0;
}

/**
 * setup() reads in the next command line, separating it into distinct
 * tokens using whitespace (space or tab) as delimiters. setup() sets
 * the args parameter as a null-terminated string.
 */
int setup(char inputBuffer[], char *args[], int *background)
{
  int length, /* # of characters in the command line */
    i,        /* loop index for accessing inputBuffer array */
    start,    /* index where beginning of next command parameter is */
    ct;       /* index of where to place the next parameter into args[] */
  ct = 0;

  /* read what the user enters on the command line */
  length = read(STDIN_FILENO, inputBuffer, MAX_LINE);
  inputBuffer[length] = '\0';
  start = -1;
  if (length == 0) {
    /* ctrl-d was entered, quit the shell normally */
    printf("\n");
    exit(0);
  }
  if (length < 0) {
    /* somthing wrong; terminate with error code of -1 */
    perror("Reading the command");
    exit(-1);
  }
  //printf("length: %d\n", length);
  if ((length == 2 || length ==4) && inputBuffer[0] == 'r') { // handle the case when only "r\n" is entered
	 tmp = (char*)malloc(MAX_LINE);
	 if (length == 2)
	   tmp = get_cmd('\0');
	 else
	   tmp = get_cmd(inputBuffer[2]);
	 if (tmp == NULL) {
	  	 printf("No command in history found\n");
	  	 return -1;
	 } else {
	 	  strcpy(inputBuffer, tmp);
		  length = strlen(inputBuffer);
	 }
  }

  // copy inputBuffer to tmp string (global) and add it to history arraylist
  tmp = (char*)malloc(MAX_LINE);
  if (NULL != tmp) {
	  strcpy(tmp, inputBuffer);
	  arraylist_add(&history, tmp);
  }

  /* examine every character in the inputBuffer */
  for (i = 0; i < length; i++) {
    switch (inputBuffer[i]){
    case ' ':
    case '\t':               /* argument separators */
      if(start != -1){
        args[ct] = &inputBuffer[start];    /* set up pointer */
        ct++;
      }
      inputBuffer[i] = '\0'; /* add a null char; make a C string */
      start = -1;
      break;
    case '\n':                 /* should be the final char examined */
      if (start != -1){
        args[ct] = &inputBuffer[start];
        ct++;
      }
      inputBuffer[i] = '\0';
      args[ct] = NULL; /* no more arguments to this command */
      break;
    default :             /* some other character */
      if (inputBuffer[i] == '&'){
        *background  = 1;
        inputBuffer[i] = '\0';
        start = -1; // fix suggested by Guang Yang on discussion board.
      } else if (start == -1)
        start = i;
    }
  }
  args[ct] = NULL; /* just in case the input line was > MAX_LINE */

  if (strncmp(args[0], "exit\n", 4) == 0) {
	  exit(0);
  }
  else if (strncmp(args[0], "history\n", 7) == 0) {
  	  print_history();
  	  return -1;
  }
  else if (strncmp(args[0], "pwd\n", 3) == 0) {
      abs_path();
      return -1;
  } else if (strncmp(args[0], "cd", 2) == 0) {
	  ch_dir(args[1]);
	  return -1;
  } else if (strncmp(args[0], "jobs\n", 4) == 0) {
	  get_running_process();
	  return -1;
  } else if (strncmp(args[0], "fg", 2) == 0) {
	  int par;
	  char *error;
	  // get the job number and convert to integer
	  par =	(int)strtol(args[1], &error, 10);
	  if (args[1] == error) {
	  	printf("invalid job number\n");
	  }
	  else {
		  // conversion successful, recall background process
		  if (par > arraylist_size(bg_processes) - 1) { //index 0 offset
			  printf("Job %d not available\n", par);
		  } else {
			  foreground(par);
		  }
	  }
	  return -1;
  }
  return 0;
}

void get_running_process() {
	/*
	 * we loop through arraylist of running processes. If the process is found
	 * still running, we print it. If the process is not running any longer, we
	 * remove it from the list.
	 *
	 */
	int i, status, return_pid;
	process *p;
	for (i=0; i < arraylist_size(bg_processes); i++) {
		p = arraylist_get(bg_processes, i);
		if (p != NULL) {
			return_pid = waitpid(p->id, &status, WNOHANG);
			if (return_pid == 0) {
				//process is still running. print details:
				printf("[%d]\t%s\n", i, p->cmd);
			} else {
				//process no longer running, remove it from list of running processes
				arraylist_remove(&bg_processes, i);
				// since we moved elements of the dynamic array back one index,
				// check this index again in the next iteration
				i--;
			}
		}
	}
}

void process_init(process* pss, pid_t pid, char* command) {
   char *cpy;
   cpy = (char*) malloc(strlen(command) + 1);
   strcpy(cpy, command);
   pss->id = pid;
   pss->cmd = cpy;
}

char* get_cmd(char first_char) {
	int i;
	char *return_string;
	char *cmd;
	if (arraylist_size(history) == 0)
		return NULL;
	else if (first_char == '\0') {
		return (char*)arraylist_get(history, arraylist_size(history) - 1);
	} else {
		i = arraylist_size(history) - 1;
		return_string = NULL;
		while (i >= 0 && i >= arraylist_size(history) - BUFFER) {
			   cmd = (char*)arraylist_get(history, i--);
			   if (cmd[0] == first_char) {
				   return_string = cmd;
				   break;
			   }
			}
		return return_string;
	}
}

void print_history() {
	char* cmd;
	int i = arraylist_size(history) - 1;
	while (i >= 0 && i >= arraylist_size(history) - BUFFER) {
		cmd = (char*)arraylist_get(history, i);
		printf("%d\t%s", (i--) + 1, cmd);
	}
}

void abs_path() {
   char *buf;
   long size;
   size = pathconf(".", _PC_PATH_MAX);
   //printf("max path size  is: %d\n", size);
   buf = malloc(size);
   if (buf != NULL)
      getcwd(buf, size);
   else
      printf("Unable to get path.");
   if (buf == NULL)
      printf("ERROR: %s\n", strerror(errno));
   else
      printf("%s\n", buf);
}

void ch_dir(char* path) {
   if (path == NULL) {
	  path = "~/";
   }
   if(!chdir(path)) {
	   printf("Changed directory to %s\n", path);
   } else {
	   printf("ERROR: %s\n:", strerror(errno));
   }
}

void foreground(int i) {
	int status;
	process *p;
	pid_t return_pid;

	p = arraylist_get(bg_processes, i);

	return_pid = waitpid(p->id, &status, WNOHANG);

	if (return_pid == -1) {
		error("unable to retrieve the job\n");
	} else if (return_pid == 0) {
		printf("\n%s\n", p->cmd);
		return_pid = waitpid(p->id, &status, 0);
		if (return_pid == -1) {
			error("Unable to bring job to foreground");
		}
	} else {
		printf("Process is no longer running");
		arraylist_remove(&bg_processes, i);
	}
}
