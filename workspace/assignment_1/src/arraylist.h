/*
 * arraylist.h
 *
 * Author: Abdul Haseeb Jehangir
 * Date: September 26, 2014
 * ECSE 427, FALL 2014
 *
 */

#ifndef ARRAYLIST_H_
#define ARRAYLIST_H_

#define INITIAL_LENGTH 2

typedef struct arraylist {
	int size;
	int max_length;
	void **strings;
} arraylist;

extern void arraylist_init(arraylist *list);
extern int arraylist_size(arraylist list);
extern void* arraylist_get(arraylist list, int index);
extern void arraylist_add(arraylist *list, void *data);
void arraylist_remove(arraylist* list, int index);
// Unit tests
extern void arraylist_test(void);

#endif /* ARRAYLIST_H_ */
