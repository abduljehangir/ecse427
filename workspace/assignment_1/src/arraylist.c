/*/*
 * arraylist.c
 *
 * Author: Abdul Haseeb Jehangir
 * Date: September 26, 2014
 * ECSE 427, FALL 2014
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include "arraylist.h"

void arraylist_init(arraylist *list)
{
	list->size = 0;
	list->max_length = INITIAL_LENGTH;
	list->strings = (void**) malloc(sizeof(void*)*INITIAL_LENGTH);
}

int arraylist_size(arraylist list)
{
	return list.size;
}

void* arraylist_get(arraylist list, int index)
{
	if (index < list.size) {
		return list.strings[index];
	} else {
		return NULL;
	}
}

void arraylist_add(arraylist *list, void *data)
{
	void **newptr;
	if (data != NULL && list != NULL) {
		newptr = list->strings;
		if (list->size == list->max_length) {
			newptr = realloc(list->strings, 2 * list->max_length * sizeof(void*));
			if (newptr == NULL) {
				printf("unable to allocate more memory to arraylist. Arraylist add failed.");
				return;
			} else {
				list->max_length *= 2;
			}
		}
		if (newptr != NULL) {
			list->strings = newptr;
			list->strings[list->size++] = data;
		}
	}
}

void arraylist_remove(arraylist* list, int index) {
	void **newptr;
	//printf("trying to remove");
	int i;
	if (NULL != list) {
		if (index >= list->size) {
			printf("Index not available in arraylist. Unsuccessful");
		}
		//move back other elements one index.
		for (i = index; i < list->size - 1;) {
			list->strings[i] = list->strings[++i];
		}
		list->size = list->size--;
		//reallocate the array with a less size if the size is too big.
		newptr = list->strings;
		if (list->size * 2 < list->max_length) {
			newptr = realloc(list->strings, list->max_length * sizeof(void*) / 2);
			if (NULL != newptr) {
				list->max_length /= 2;
			}
		}
		if (newptr != NULL) {
			list->strings = newptr;
		}
	}
}

void arraylist_test()
{
	arraylist list;
	arraylist_init(&list);

	arraylist_add(&list, "Hello!");
	arraylist_add(&list, "This is an ArrayList!");
	// add first two elements. Memory is already allocated
	//printf("Size of arraylist: %d\n", arraylist_size(list));
	//printf("Arraylist elements:\n%s\n%s\n", (char*)arraylist_get(list, 0),
	//										(char*)arraylist_get(list, 1));
	// now we add more elements so arraylist will get more memory allocated.
	arraylist_add(&list, "I am element 3!");
	arraylist_add(&list, "I am element 4!");
	printf("Size of arraylist: %d\n", arraylist_size(list));
	printf("arraylist max size: %d\n", list.max_length);

	// check array size and elements now!
	//printf("Size of arraylist: %d\n", arraylist_size(list));
	//printf("Arraylist elements:\n%s\n%s\n%s\n%s\n", arraylist_get(list, 0),
	//												arraylist_get(list, 1),
	//												arraylist_get(list, 2),
	//												arraylist_get(list, 3));

	arraylist_remove(&list, 0);
	arraylist_remove(&list, 0);
	arraylist_remove(&list, 0);
	printf("Arraylist elements:\n%s\n", arraylist_get(list, 0));
	printf("arraylist size: %d\n", list.size);
	printf("arraylist max size: %d\n", list.max_length);


}
