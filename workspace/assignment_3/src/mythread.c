/*
 * mythread.c
 *
 *  Created on: Nov 8, 2014
 *      Author: abdulhaseeb
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include "mythread.h"

void print_error(Error e);
void set_timer();
void switch_context(void);
char* thread_tostring(Mythread_control_block* thread);

struct itimerval tval;

u32 threads_created;
u32 semaphores_created;
u32 quantum;
Queue run_queue;
Mythread_control_block thread_table[MAX_THREADS];
Semaphore sem_table[MAX_SEMAPHORE];


/**
 * @brief:	This function initiates mythread library
 */

int mythread_init() {
	// initialize counters
	threads_created = 0;
	semaphores_created = 0;
	// initialize properties
	assert(DEFAULT_QUANTUM >= MIN_QUANTUM);
	quantum = DEFAULT_QUANTUM;
	//set_timer();
	//sigset(SIGALRM, sig_hand);
	//initialize queues
	queue_init(&run_queue);
	return 0;
}

/**
 * @brief:	This function creates a new thread.
 * @param:	thread name
 * @param:	function pointer to the function to run in this thread
 * @param:	stack size to use for this thread
 * @param:	input argument for the thread function
 */
int mythread_create(char *threadname, void (*threadfunc)(), int stacksize, int arg) {
	if (MAX_THREADS == threads_created) {
		print_error(THREAD_CREATE_FAILED);
		return -1; /* unable to create a new thread. non-zero exit status. */
	}
	// create a new thread

	//allocate stack for the thread on the heap
	char* stack = (char*)malloc(stacksize);

	if(getcontext(&(thread_table[threads_created].context)) == -1) {
		print_error(THREAD_CREATE_FAILED);
		return -1;
	}

	// Create a new thread control block
	thread_table[threads_created].context.uc_link = NULL;
	thread_table[threads_created].context.uc_stack.ss_sp = stack;
	thread_table[threads_created].context.uc_stack.ss_size = stacksize;

	thread_table[threads_created].thread_id = threads_created;
	thread_table[threads_created].thread_name = threadname;
	thread_table[threads_created].thread_status = RUNNABLE;

	// make the context
	makecontext(&(thread_table[threads_created].context), threadfunc, 1, arg);

	// print the tread name for information purposes.
	printf("New thread created: %s\n", threadname);

	// append this control block at the end of run queue
	queue_enque(&run_queue, threads_created++);

	return threads_created - 1;
}

/**
 * @brief:	This function exits/kills a thread. It will take it off the run_queue
 */
void mythread_exit() {

	if (queue_deque(&run_queue) <= 1) {
		print_error(THREAD_EXIT_ERR);
		exit(0);
	} else {
		int dying_thread;
		// remove the thread from the run queue
		dying_thread = queue_deque(&run_queue);
		//free the memory in heap for the dying thread
		free(thread_table[dying_thread].context.uc_stack.ss_sp);
		// Set the thread status is exit mode in thread table
		thread_table[dying_thread].thread_status = EXIT;
		// now switch to the next thread on run_queue
		thread_table[queue_peek(&run_queue)].thread_status = RUNNING;
		set_timer();
		swapcontext(&(thread_table[dying_thread].context),
				&(thread_table[queue_peek(&run_queue)].context));
	}
}

/**
 * @brief:	This function starts the thread execution. Context is switched to
 * 			another thread. Also enables context switching.
 */
void runthreads() {

	if (queue_size(&run_queue) > 0) {
		thread_table[queue_peek(&run_queue)].thread_status = RUNNING;
		//enable thread switcher
		set_timer();

		// Main thread should also be on the thread table so it gets its chance
		// to be executed.
		if(getcontext(&(thread_table[threads_created].context)) == -1) {
			print_error(THREAD_CREATE_FAILED);
			exit(0);
		}
		thread_table[threads_created].thread_status = RUNNABLE;
		thread_table[threads_created].thread_name = "Main_thread";
		thread_table[threads_created].cpu_time = 0;
		thread_table[threads_created].thread_id = threads_created;
		// append this control block at the end of run queue
		queue_enque(&run_queue, threads_created);

		sigset(SIGALRM, switch_context);
		if (swapcontext(&(thread_table[threads_created++].context),
				&(thread_table[queue_peek(&run_queue)].context)) == -1) {
			fprintf(stderr, "error swapping context");
		}
	}
}

/**
 * @brief:	Set a new quantum size.
 * @param:	quantum size in microseconds.
 */
void set_quantum_size(int number) {
	// make sure number is appropriate
	assert (number >= MIN_QUANTUM);
	quantum = number;
}

/**
 * @brief:	Creates a new semaphore
 * @param:	initial value of semaphore
 * @return: semaphore ID for future reference. This is the ID to be used later
 * 			to wait/signal the semaphore.
 */
int create_semaphore(int value) {
	// take the next available semaphore from the table
	sem_table[semaphores_created].initial_value = value;
	sem_table[semaphores_created].value = value;
	sem_table[semaphores_created].status = INUSE;
	// initialize semaphore queue
	queue_init(&(sem_table[semaphores_created++].q));

	return semaphores_created - 1;
}

/**
 * @brief:	Decrements the count for the semaphore
 * @param:	The referred semaphore ID to apply the wait on
 */
void semaphore_wait(int sem) {
	// block SIGALRM to disable context switching
	sigset_t sset, oldset;
	struct itimerval timer;

	sigemptyset(&sset);
	sigaddset(&sset, SIGALRM);
	sigprocmask(SIG_BLOCK, &sset, &oldset);

	// do the semaphore wait business now
	Semaphore* sem_st = &sem_table[sem];
	Mythread_control_block *blocked_thread, *next_thread;

	if (sem_st->status == INUSE) {
		sem_st->value -= 1;
		if (sem_st->value < 0) {
			//should block running thread. deque from the run_queue
			blocked_thread = &(thread_table[queue_deque(&run_queue)]);
			// set the status to blocked and add to semaphore queue.
			blocked_thread->thread_status = BLOCKED;
			queue_enque(&(sem_st->q), blocked_thread->thread_id);

			/* what if there is no other thread???? well in my defense, you
			  really shouldn't be using semaphores if you have just one thread.
			  there should some one other thread present to signal the semaphore.
			  I will exit the program incase there is no other process and print
			  an error on stderr.
			 */
			if (queue_size(&run_queue) > 0)
				next_thread = &(thread_table[queue_peek(&run_queue)]);
			else {
				//print error and exit
				print_error(SEM_SWITCH_ERR);
				exit(0);
			}

			// set the next thread as running.
			next_thread->thread_status = RUNNING;

			// save the amount of time executed on the cpu for the outgoing process
			getitimer(ITIMER_REAL, &timer);
			blocked_thread->cpu_time += quantum - timer.it_interval.tv_usec;
			// restart the timer for the next thread
			set_timer();
			/*
			 * assuming sig mask is for the process and not limited to each
			 * thread/context, we need to unblock SIGALRM
			 */
			sigprocmask(SIG_SETMASK, &oldset, 0);
			//manually switch to next thread.
			swapcontext(&(blocked_thread->context), &(next_thread->context));
		}
	} else {
		print_error(SEM_USAGE_ERR);
		exit(0);
	}
	// enable SIGALRM again for future context switching
	sigprocmask(SIG_SETMASK, &oldset, 0);
}

/**
 * @brief:	Increments the count value of semaphore by 1. If a thread was waiting
 * 			on queue, it is added on the run queue.
 * @param:	The referred semaphore ID to apply the signal on.
 */
void semaphore_signal(int sem) {
	// block SIGALRM to disable context switching
	sigset_t sset, oldset;
	sigemptyset(&sset);
	sigaddset(&sset, SIGALRM);
	sigprocmask(SIG_BLOCK, &sset, &oldset);
	// do the semaphore wait business now
	int thrd_id;
	Semaphore *sem_st = &sem_table[sem];
	if (sem_st->status == INUSE) {
		sem_st->value += 1;

		if (sem_st->value <= 0) {
			// let one thread go!
			thrd_id = queue_deque(&sem_st->q);
			// set it to runnable and add to the end of run queue
			thread_table[thrd_id].thread_status = RUNNABLE;
			queue_enque(&run_queue, thrd_id);
		}
	} else {
		print_error(SEM_USAGE_ERR);
	}
	// enable SIGALRM again for future context switching
	sigprocmask(SIG_SETMASK, &oldset, 0);
}

/**
 * @brief:	Destroys the semaphore. Semaphore is not usable after this point
 * @param:	Semaphore ID to be destroyed.
 */
void destroy_semaphore(int sem) {
	Semaphore *outgoing = &(sem_table[sem]);
	// check if there are any threads waiting on the semaphore
	if (queue_size(&outgoing->q) > 0) {
		print_error(SEM_DELETE_ERR);
	} else {
		//destroy the semaphore
		if (outgoing->value != outgoing->initial_value) {
			print_error(SEM_DELETE_WARNING);
		}
		// make it unusable
		outgoing->status = DESTROYED;
	}
}

/**
 * @brief:	Prints the status and CPU time of each thread handled by the library.
 */
void mythread_state() {
	int i;
	printf("***************************************************************\n");
	printf("THREAD NAME\t\tTHREAD STATUS\t\tCPU TIME (us)\n");
	for (i = 0; i < threads_created; i++) {
		printf("%s\t\t%s\t\t%d\n", thread_table[i].thread_name,
				thread_tostring(&thread_table[i]), thread_table[i].cpu_time);
	}
	printf("***************************************************************\n");
}

/**
 * @brief:	SIGALRM hander for switching context between each thread.
 */
void switch_context() {
	// Add quantum time to current process' cpu time since it finished one cycle
	thread_table[queue_peek(&run_queue)].cpu_time += quantum;
	// if there are multiple threads to work on, then switch between them
	if (queue_size(&run_queue) - 1 > 0) {
		int outgoing_id = queue_deque(&run_queue);
		int incoming_id = queue_peek(&run_queue);
		queue_enque(&run_queue, outgoing_id);
		(thread_table[outgoing_id]).thread_status = RUNNABLE;
		(thread_table[incoming_id]).thread_status = RUNNING;
		swapcontext(&(thread_table[outgoing_id].context), &(thread_table[incoming_id].context));
	}
}

/**
 * @brief:	Resets the timer value back to the value defined by quantum.
 */
void set_timer() {
	tval.it_interval.tv_sec = 0;
	tval.it_interval.tv_usec = quantum;
	tval.it_value.tv_sec = 0;
	tval.it_value.tv_usec = quantum;
	setitimer(ITIMER_REAL, &tval, 0);
}

/**
 * @brief:	Returns a string representation of a thread control block. The info
 * 			includes the thread status and its CPU execution time.
 * @param:	pointer to the thread control block for which info is required.
 */
char* thread_tostring(Mythread_control_block* thread) {
	char* status;
	switch(thread->thread_status) {
	case RUNNABLE:
		status = "RUNNABLE";
		break;
	case RUNNING:
		status = "RUNNING";
		break;
	case BLOCKED:
		status = "BLOCKED";
		break;
	case EXIT:
		status = "EXIT";
		break;
	}
	return status;
}

/**
 * @brief:	Prints the error msg on strerr.
 * @param:	ERROR CODE
 */
void print_error(Error e) {
	switch (e) {
	case THREAD_CREATE_FAILED:
		fprintf(stderr, "Thread creation failed\n");
		break;
	case SEM_DELETE_ERR:
		fprintf(stderr, "Threads in queue, unable to delete semaphore.\n");
		break;
	case SEM_DELETE_WARNING:
		fprintf(stderr, "Semaphore final value not equal to its initial value.\n");
		break;
	case SEM_USAGE_ERR:
		fprintf(stderr, "Semaphore is not defined or is destroyed.\n");
		break;
	case SEM_SWITCH_ERR:
		fprintf(stderr, "Wait on semaphore requested but no other thread to run.\n");
		break;
	case THREAD_EXIT_ERR:
		fprintf(stderr, "No other thread available to run. Exiting...\n");
		break;
	default:
		break;
	}
}
