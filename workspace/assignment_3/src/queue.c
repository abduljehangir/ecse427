/*
 * queue.c
 *
 *  Created on: Nov 8, 2014
 *      Author: abdulhaseeb
 */

#include "queue.h"

void queue_init(Queue* q) {
	q->l = list_create(NULL);
	q->size = 0;
}

void queue_enque(Queue* q, int item) {
	q->l = list_append_int(q->l, item);
	q->size = q->size + 1;
}

int queue_deque(Queue* q) {
	int value;
	value = list_shift_int(q->l);
	if (value != NULL)
		q->size = q->size - 1;
	return value;
}

int queue_peek(Queue *q) {
	return list_item_int(q->l, 0);
}

int queue_size(Queue* q) {
	return q->size;
}
