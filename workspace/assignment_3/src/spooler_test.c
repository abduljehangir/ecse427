/*
 * spooler_test.c
 *
 *  Created on: Oct 12, 2014
 *      Author: abdulhaseeb
 *      ID:		260399461
 *  cloned from spooler.c
 *  Last Edited: Nov 13, 2014
 *  Edit: use mythread.h library
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "circular_buffer.h"
#include "mythread.h"

/* Type definitions ********************************************************* */
typedef unsigned short u16;

typedef struct node {
	int id;			// numerical identifier
	int tid;  // thread id
} node;

void node_init(node* n, u16 i) {
	n->id = i;
}
/**************************************************************************** */
#define MAX_PAGES 			10	/* Max number of pages a client can print */
#define CLIENT_WAIT_TIME	6	/* Number of seconds each client will wait before putting another request */

/* global variables */
cir_buffer buf;					/* Circular Buffer */
int buf_sem;					/* Semaphore for accessing circular buffer*/
int overflow_sem;				/* Semaphore for overflow non-busy wait */
int underflow_sem;			/* Semaphore for underflow non-busy wait */
pthread_mutex_t mutex;		/* mutex to lock counters while change/access */

int printers_waiting;		/* Number of printers waiting */
int clients_waiting;		/* Number of clients waiting */
long limit = 99900000;

static void wait(long wait_time) {
	long l;
	for (l = 0; l < wait_time;){
		l++;
	}
}

void incr(int *var) {
	pthread_mutex_lock(&mutex);
	*var += 1;
	pthread_mutex_unlock(&mutex);
}

void decr(int *var) {
	pthread_mutex_lock(&mutex);
	*var -= 1;
	pthread_mutex_unlock(&mutex);
}

int get_value(int *var) {
	int value;
	pthread_mutex_lock(&mutex);
	value = *var;
	pthread_mutex_unlock(&mutex);
	return value;
}

/**
 * @brief:	This function implements client code to generate print jobs.
 * @param:	Client unique ID
 * @note:	Uses semaphores to safely access circular buffer. Moreover implements
 * 			non-busy waits using semaphores when buffer is full. Client will run
 * 			forever.
 */
void* client(int my_id) {

	int pages_cnt, page_buffer, index, client_wait;
	client_wait = 0;
	//client will generate jobs forever
	while(1) {
		// produce job. using random number of pages between 1 and MAX_PAGES
		pages_cnt = 1 + rand() % MAX_PAGES;

		if (client_wait) {
			// if wait flag was raised, then wait until we get our turn to put
			// job on the buffer
			incr(&clients_waiting);
			semaphore_wait(overflow_sem);
			printf("Client %d wakes up.\n", my_id);
			// recall the number of pages client was supposed to print
			pages_cnt = page_buffer;
			client_wait = 0;
		}
		//wait till client gets it turn to access buffer
		semaphore_wait(buf_sem);
		// put the job in the buffer
		index = buf_add_job(&buf, pages_cnt);
		//if the buffer was full then do this
		if (index == -1) {
			printf("Client %d has %d pages to print, buffer full, sleeps\n",
					my_id, pages_cnt);
			//store the job in register to retrieve on wake up call
			page_buffer = pages_cnt;
			// raise flag to indicate i am waiting to get spot on the buffer.
			client_wait = 1;
			// let others access the buffer
			semaphore_signal(buf_sem);

		} else {
			// if the buffer was not full then job was successfully put on buffer
			printf("Client %d has %d pages to print, puts request in Buffer[%d]\n",
					my_id, pages_cnt, index);

			//if atleast one printer was waiting, wake up one printer
			if (get_value(&printers_waiting)) {
				semaphore_signal(underflow_sem);
				decr(&printers_waiting);
			}
			// let others use the buffer.
			semaphore_signal(buf_sem);
			// sleep for some while, then create another job.
			wait(CLIENT_WAIT_TIME * limit);
		}
	}
	return NULL;
}

/**
 * @brief:	This function implements printer code to print jobs in buffer.
 * @param:	Printer unique ID
 * @note:	Uses semaphore to safely access circular buffer. Moreover implements
 * 			non-busy waits when no jobs available for printing in buffer. Printer
 * 			will run forever.
 */
void* printer(int my_id) {
	int pages_cnt, index;
	sleep(1);
	while(1) {
		// wait for turn to access the buffer.
		semaphore_wait(buf_sem);
		if (buf_empty(&buf)) {
			// if the buffer is empty, then oh well, wait for jobs to become
			// available. client will send wake up call if jobs become available.
			printf("No request in buffer. Printer %d sleeps\n", my_id);
			// let others use the buffer
			semaphore_signal(buf_sem);
			// wait for client to send signal to wake up
			incr(&printers_waiting);
			semaphore_wait(underflow_sem);

			//printf("Printers waiting: %d\n", get_value(&printers_waiting));
		} else {
			// remove a job from the buffer
			index = buf_get_job(&buf, &pages_cnt);
			if (get_value(&clients_waiting)) {
				// if the buffer was full and a client was waiting, a job is
				// now removed. Send client the signal that a spot is now
				// available on the buffer.
				semaphore_signal(overflow_sem);
				decr(&clients_waiting);
			}

			printf("Printer %d starts printing %d pages from Buffer[%d]\n",
					my_id, pages_cnt, index);
			// let others use the buffer.
			semaphore_signal(buf_sem);
			// wait 1 second per page;
			wait(pages_cnt*limit);
			printf("Printer %d finishes printing %d pages from Buffer[%d]\n",
								my_id, pages_cnt, index);
		}
	}
	return NULL;
}

void thread_printer(int i) {
	// dummy variable
	i = 3;
	while(1) {
		mythread_state();
		wait(5*limit);
	}
}
/********************************************************************
 *
 * MAIN
 * Usage ./a.out <C> <P> <B>\n
 * Assumptions: C, P and B are integer numbers > 0
 * This program fails to handle one corner case at the very beginning.
 *
 ********************************************************************/
int main(int argc, char* argv[]) {
	int client_cnt, printer_cnt, buf_size;
	u16 i;

	if(argc != 4) {
		printf("Usage ./a.out <C> <P> <B>\n");
		exit(0);
	}

	// Assuming user will enter numbers. Defensive programming not a real
	// purpose of this assignment!
	client_cnt = atoi(argv[1]);
	printer_cnt = atoi(argv[2]);
	buf_size = atoi(argv[3]);

	// make arrays of clients and printers to store unique IDs and thread IDs

	node clients[client_cnt];
	node printers[printer_cnt];

	buf_init(&buf, buf_size); // initialize circular buffer

	// semaphore for accessing buffer. Only one node can access buffer at a time
	//sem_init(&buf_sem, 0, 1);
	buf_sem = create_semaphore(1);
	// semaphores for over and underflow.
	//sem_init(&overflow_sem, 0, 0);
	overflow_sem = create_semaphore(0);
	//sem_init(&underflow_sem, 0, 0);
	underflow_sem = create_semaphore(0);
	printers_waiting = 0;
	clients_waiting = 0;
	mythread_init();

	char str_clients[client_cnt][20];
	char str_printers[printer_cnt][20];

	// create client threads. one thread per client
	for (i = 0; i < client_cnt; i++) {
		// Initiate client struct and assign an ID

		snprintf(str_clients[i], 20, "%s%d", "client ", i);
		node_init(&clients[i], i);

		clients[i].tid = mythread_create(&str_clients[i][0], client, 16384, clients[i].id);
	}
	// create printer threads. one thread per printer.
	for (i = 0; i < printer_cnt; i++) {
			 //Initiate client struct and assign an ID
		node_init(&printers[i], i);
		snprintf(str_printers[i], 20, "%s%d", "printer ", i);
		printers[i].tid = mythread_create(&str_printers[i][0], printer, 16384, printers[i].id);
	}
	mythread_create("thread_states", thread_printer, 16384, 100);
	runthreads();
	// Program will never end.
	while(1);
	return 0;
}
