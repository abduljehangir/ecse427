Student Name: Abdul Haseeb Jehangir
Student ID:   260399461

The assignment is complete. It compiled and ran OK on ubuntu 14.04 LTS on a
personal PC. I dont see any reason why it will not run on Trottier 3rd floor.
It provides the following API:


/****************************************************************************
  API
 ***************************************************************************/
int mythread_init(void);
int mythread_create(char *threadname, void (*threadfunc)(), int stacksize, int arg);
void mythread_exit(void);
void runthreads(void);
void set_quantum_size(int time);
int create_semaphore(int value);
void semaphore_wait(int semaphore);
void semaphore_signal(int semaphore);
void destroy_semaphore(int semaphore);
void mythread_state(void);

/****************************************************************************
  COMPILE INSTRUCTIONS
 ***************************************************************************/
>> cd ./src/
>> gcc  circular_buffer.c mythread.c queue.c spooler_test.c -I ../inc/ -o assignment3 -DHAVE_PTHREAD_RWLOCK=1 -lslack

/****************************************************************************
  EXECUTION INSTRUCTIONS
 ***************************************************************************/
>> Usage ./assignment3 <C> <P> <B>

/****************************************************************************
  NOTES AND LIMITATIONS
 ***************************************************************************/

1) sleep function will not work in any function. The reason for this is that
   the library uses SIGALRM to make context switches. It seems like if the
   sleep function is running and context switch is made, the sleep call is
   cancelled. I asked this question to the professor and he said this behavior
   is acceptable for the purpose of this assignment.
   However, in order to force sleeps in my functions, I added some dummy for
   loops in my threads for the purpose of testing.
2) If you notice above, mythread_create function is not following the proposed
   API in the assignment specification. It takes in a variable of type int. This
   parameter is the input variable for the defined thread function. I tried to
   incorporate support for dynamic number of parameters. However, C doesnot
   provide a safe way to have dynamic input parameter for a function. I tried
   using stdarg.h library in order to use va_list. However, makecontext function
   does not support va_list.
   Having said that, I decided to leave this API to only support function with
   one input paramter. WHY I CHOOSE 1 PARAMETER? well read the next point. 
   Moreover, I asked the professor if we are allowed to change the API. His
   answer was YES, as long as it is document here.
3) The program was tested with my submission of assignment 2 code since I scored
   100/100 on it. Moreover, it aimed directly at semaphores and threads so it
   looks like a good program to test functionality of semaphores and context
   switching. The test code is located in spooler_test.c
   Both printer and client functions take in one int argument. Hence my choice
   to adjust the API to create threads with one input parameter.
   Some changes were required in order to use mythread library. All semaphore
   instructions were replaced with equivalent semaphore instruction from mythread.h.
   All pthread instructions were replaced with equivalent thread instructions in
   mythread.h. There are a couple of places where I used mutex in assignment 2.
   Since mythread.h API does not provide support for mutex, I didnot change the
   mutex part of things is my test.
   The output of my test program is correct and is listed at the end.
4) All my type definition, function and variables follow this convention:
      typedef: Status, Queue,... (starts with a capital letter)
      variable: int i, long letter, ... (starts with a small letter)
      function: functions start with a small letter
5) There are a few assumptions that were made:
   (i) if semaphore_wait call is made, and there is a need for context switch,
      BUT there are no more processes on the run_queue, then the program will
      exit and after printing error message on stderr. This is a defensive
      approach, since the user shouldnt really be calling semaphore wait unless
      there is another RUNNABLE thread present who will unlock the semaphore.
   (ii) Upon deletion of a thread, if there is no other RUNNABLE thread, the
      program will exit.
   (iii) If a semaphore is attempted to use which is not yet initialized, or, is
      in destroyed state, the program will exit with an error message.
6)  Minimum quatum is is forced to be 20 microseconds. The program was tested
    with this value was functioned correctly. The values below this mey work but
    are not tested. Assert commands will have to be removed in set_quantum_size
    and mythread_init functions.
      
/****************************************************************************
  OUTPUT
 ***************************************************************************/
Here is the output from the spooler_test.c (assignment 2) with assignment 3
mythread.h library.
There are 6 clients, 2 printers and buffer size is 3


New thread created: client 0
New thread created: client 1
New thread created: client 2
New thread created: client 3
New thread created: client 4
New thread created: client 5
New thread created: printer 0
New thread created: printer 1
New thread created: thread_states
Client 0 has 4 pages to print, puts request in Buffer[0]
Client 1 has 7 pages to print, puts request in Buffer[1]
***************************************************************
THREAD NAME		THREAD STATUS		CPU TIME (us)
client 0		RUNNABLE		1000
client 1		RUNNABLE		1000
client 2		BLOCKED		0
client 3		BLOCKED		0
client 4		BLOCKED		0
client 5		BLOCKED		0
printer 0		RUNNABLE		1000
printer 1		RUNNABLE		1000
thread_states		RUNNING		0
Main_thread		RUNNABLE		0
***************************************************************
Client 2 has 8 pages to print, puts request in Buffer[2]
Client 3 has 6 pages to print, buffer full, sleeps
Client 4 has 4 pages to print, buffer full, sleeps
Client 5 has 6 pages to print, buffer full, sleeps
Printer 0 starts printing 4 pages from Buffer[0]
Client 3 wakes up.
Printer 1 starts printing 7 pages from Buffer[1]
Client 4 wakes up.
Client 3 has 6 pages to print, puts request in Buffer[0]
Client 4 has 4 pages to print, puts request in Buffer[1]
Printer 0 finishes printing 4 pages from Buffer[0]
Printer 0 starts printing 8 pages from Buffer[2]
Printer 0 starts printing 8 pages from Buffer[2]
Client 5 wakes up.
Client 5 has 6 pages to print, puts request in Buffer[2]
***************************************************************
THREAD NAME		THREAD STATUS		CPU TIME (us)
client 0		RUNNABLE		1571000
client 1		RUNNABLE		1571000
client 2		RUNNABLE		1569000
client 3		RUNNABLE		1561000
client 4		RUNNABLE		1561000
client 5		RUNNABLE		376000
printer 0		RUNNABLE		1563000
printer 1		RUNNABLE		1562000
thread_states		RUNNING		1570000
Main_thread		RUNNABLE		1570000
***************************************************************
Client 0 has 2 pages to print, buffer full, sleeps
Client 4 has 10 pages to print, buffer full, sleeps
Client 3 has 7 pages to print, buffer full, sleeps
Client 1 has 7 pages to print, buffer full, sleeps
Printer 1 finishes printing 7 pages from Buffer[1]
Printer 1 starts printing 6 pages from Buffer[0]
Client 0 wakes up.
Client 0 has 2 pages to print, puts request in Buffer[0]
Client 5 has 7 pages to print, buffer full, sleeps
***************************************************************
THREAD NAME		THREAD STATUS		CPU TIME (us)
client 0		RUNNABLE		2833000
client 1		BLOCKED		1904000
client 2		BLOCKED		1863000
client 3		BLOCKED		1891000
client 4		BLOCKED		1863000
client 5		BLOCKED		1884000
printer 0		RUNNABLE		3145000
printer 1		RUNNABLE		3144000
thread_states		RUNNING		3153000
Main_thread		RUNNABLE		3154000
***************************************************************
Printer 0 starts printing 4 pages from Buffer[1]
Client 2 wakes up.
Client 2 has 8 pages to print, puts request in Buffer[1]
Printer 1 finishes printing 6 pages from Buffer[0]
Printer 1 starts printing 6 pages from Buffer[2]
Client 4 wakes up.
Client 4 has 10 pages to print, puts request in Buffer[2]
Client 0 has 9 pages to print, buffer full, sleeps
***************************************************************
THREAD NAME		THREAD STATUS		CPU TIME (us)
client 0		BLOCKED		3729000
client 1		BLOCKED		1904000
client 2		RUNNABLE		2882000
client  		BLOCKED		1891000
client 4		RUNNABLE		2565000
client 5		BLOCKED		1884000
printer 0		RUNNABLE		4709000
printer 1		RUNNABLE		4708000
thread_states		RUNNING		4716000
Main_thread		RUNNABLE		4716000
***************************************************************
Printer 0 finishes printing 4 pages from Buffer[1]
Printer 0 starts printing 2 pages from Buffer[0]
Client 3 wakes up.
Client 3 has 7 pages to print, puts request in Buffer[0]

