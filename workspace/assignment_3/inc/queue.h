/*
 * queue.h
 *
 *  Created on: Nov 8, 2014
 *      Author: abdulhaseeb
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include <slack/list.h>
#include <slack/std.h>

typedef struct _queue {
	List *l;
	int size;
} Queue;

void queue_init(Queue* q);
void queue_enque(Queue* q, int item);
int queue_deque(Queue* q);
int queue_size(Queue* q);
int queue_peek(Queue *q);

#endif /* QUEUE_H_ */
