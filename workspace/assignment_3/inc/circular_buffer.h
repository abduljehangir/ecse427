/*
 * circular_buffer.h
 *
 *  Created on: Oct 12, 2014
 *      Author: abdulhaseeb
 *      ID:		260399461
 */

#ifndef CIRCULAR_BUFFER_H_
#define CIRCULAR_BUFFER_H_

typedef struct cir_buffer {
	int i;
	int j;
	int size;
	int used;
	int *jobs;
} cir_buffer;

void buf_init(cir_buffer *buf, int s);
int buf_full(cir_buffer *buf);
int buf_empty(cir_buffer *buf);
int buf_get_job(cir_buffer *buf, int* pages);
int buf_add_job(cir_buffer *buf, int pages);

#endif /* CIRCULAR_BUFFER_H_ */
