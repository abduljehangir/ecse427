/*
 * mythread.h
 *
 *  Created on: Nov 8, 2014
 *      Author: abdulhaseeb
 */

#ifndef MYTHREAD_H_
#define MYTHREAD_H_

#include <ucontext.h>
#include <stdarg.h>
#include "queue.h"

#define THREAD_NAME_LEN	128		/* Max thread name length */
#define MAX_THREADS 	16	/* Max number of supported threads */
#define MAX_SEMAPHORE	32	/* Max number of semaphores */
#define DEFAULT_QUANTUM	1000
#define MIN_QUANTUM		20

/*
 * Enumerations
 */
typedef enum {RUNNING, RUNNABLE, BLOCKED, EXIT} Thread_status;
typedef enum {USABLE, INUSE, DESTROYED} Semaphore_status;
typedef enum {THREAD_CREATE_FAILED, THREAD_EXIT_ERR, SEM_DELETE_ERR,
				SEM_DELETE_WARNING, SEM_USAGE_ERR, SEM_SWITCH_ERR} Error;

/*
 * Type Definitions
 */

typedef unsigned int u32;

typedef struct _semaphore {
	int initial_value;
	int value;
	Semaphore_status status;
	Queue q;
} Semaphore;

typedef struct _mythread_control_block {
	ucontext_t context;					/* saves the context */
	char* thread_name;	/* thread name */
	int thread_id;						/* globally unique thread id */
	Thread_status thread_status;
	int cpu_time;
} Mythread_control_block;

/*
 * Function Definitions
 */
int mythread_init(void);
int mythread_create(char *threadname, void (*threadfunc)(), int stacksize, int arg);
void mythread_exit(void);
void runthreads(void);
void set_quantum_size(int time);
int create_semaphore(int value);
void semaphore_wait(int semaphore);
void semaphore_signal(int semaphore);
void destroy_semaphore(int semaphore);
void mythread_state(void);


#endif /* MYTHREAD_H_ */
