/*
 * circular_buffer.c
 *
 *  Created on: Oct 12, 2014
 *      Author: abdulhaseeb
 *      ID:		260399461
 */
#include "../inc/circular_buffer.h"


void buf_init(cir_buffer *buf, int s) {
	buf->i = 0;
	buf->j = 0;
	buf->size = s;
	buf->used = 0;
	int data[s];
	buf->jobs = data;
}

int buf_full(cir_buffer *buf) {
	return buf->used == buf->size;
}

int buf_empty(cir_buffer *buf) {
	return buf->used == 0;
}

int buf_add_job(cir_buffer *buf, int pages) {
	int return_value;
	if (buf_full(buf)) {
		return -1;
	} else {
		*(buf->jobs + buf->j) = pages;
		buf->used++;
		return_value = buf->j;
		if (++buf->j == buf->size)
				buf->j = 0;
		return return_value;
	}
}

int buf_get_job(cir_buffer *buf, int* pages) {
	int return_value;
	if (buf_empty(buf)) {
		return -1;
	} else {
		*pages = *(buf->jobs + buf->i);
		return_value = buf->i;
		buf->used--;
		if (++buf->i == buf->size)
			buf->i = 0;
		return return_value;
	}
}
