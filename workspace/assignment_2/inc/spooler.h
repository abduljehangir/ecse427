/*
 * spooler.h
 *
 *  Created on: Oct 12, 2014
 *      Author: abdulhaseeb
 *      ID:		260399461
 */

#ifndef SPOOLER_H_
#define SPOOLER_H_

typedef unsigned short u16;

typedef struct node {
	u16 id;			// numerical identifier
	pthread_t tid;  // thread id
} node;

void node_init(node* n, u16 i) {
	n->id = i;
}

#endif /* SPOOLER_H_ */
