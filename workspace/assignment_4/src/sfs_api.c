/*
 * sfs_api.c
 *
 *  Created on: Jan 16, 2015
 *      Author: abdulhaseeb
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "sfs_api.h"
#include "disk_emu.h"

#define MIN(a,b) (((a)<(b))?(a):(b))

#define CAPACITY			16384	// Number of blocks on disk
#define BLOCK_SIZE			1024	// Number of bytes in a block
#define FILE_NAME_LENGTH	30		// Number of bytes for filename
#define MAX_FILES			256		// Max Number of supported files

typedef struct File_attr {
	short size; 			 	//number of bytes
	char modified[30]; 	// time when file was last modified
} File_attr;

typedef struct Folder_entry {
	char file_name[FILE_NAME_LENGTH]; // filename
	File_attr attributes;			  // file attributes
	short head_idx;					  // head of file on FAT
} Folder_entry;

typedef struct Fd_entry {
	int directory_idx;	    // index of file on directory table
	int write_ptr;			// ptr to write
	int read_ptr;			// ptr to read
} Fd_entry;

typedef struct Fat_entry {
	short next;				// index to next possible entry
} Fat_entry;

typedef struct Super_blk {
	int root_blocks;
	int fat_blocks;
	int data_blocks;
	int free_blocks;
} Super_blk;

char* disk_name = "comp310";
Super_blk super;
Folder_entry root_entries[MAX_FILES];
Fat_entry fat_entries[CAPACITY];
Fd_entry fd_table[MAX_FILES];

/*************************************************************
 * Private function definitions
 ************************************************************/
void get_date(char* date);
void init_fd_table(void);
int init_super_block(int fresh);
int init_root(int fresh);
int init_fat(int fresh);
int free_fd_spot(void);
int free_root_spot(void);
int next_free_block(void);
int sfs_fcreate(char* name);
int super_disk_update(void);
int fat_disk_update(int block_num);
int root_disk_update(int root_index);
int data_block_read(int block_num, char* buf);
int data_block_update(int block_num, char* buf);
int fd_file_status(int fileID);


/*************************************************************
 * Private functions
 ************************************************************/

void get_date(char* date) {
	time_t t = time(NULL);
	sprintf(date, "%s", asctime(localtime(&t)));
}

int init_super_block(int fresh) {
	if (fresh) {
		super.root_blocks = sizeof(Folder_entry) * MAX_FILES / BLOCK_SIZE;
		super.fat_blocks = sizeof(Fat_entry) * CAPACITY / BLOCK_SIZE;
		super.data_blocks = CAPACITY - super.root_blocks - super.fat_blocks - 1;
		super.free_blocks = super.data_blocks;
		// write to disk
		// super block is always located at the start and has a size of 1 block
		if (write_blocks(0, 1, &super) != 1) {
			fprintf(stderr, "Super block create error\n");
		}
	} else {
		// super block is always located at the start and has a size of 1 block
		if (read_blocks(0, 1, &super) != 1) {
			fprintf(stderr, "Super block read error\n");
			return -1;
		}
	}
	return 0;
}

int init_root(int fresh) {
	if (fresh) {
		// initialize each root entry
		int i, j;

		for(i = 0; i < MAX_FILES; i++) {
			root_entries[i].head_idx = -1;
			root_entries[i].attributes.size = -1;
			for (j = 0; j < FILE_NAME_LENGTH; j++) {
				root_entries[i].file_name[j] = '\0';
				root_entries[i].attributes.modified[j] = '\0';
			}
		}
		if (write_blocks(1, super.root_blocks, root_entries) != super.root_blocks) {
			fprintf(stderr, "Root entry init error\n");
			return -1;
		}
	} else {
		if (read_blocks(1, super.root_blocks, root_entries) != super.root_blocks) {
					fprintf(stderr, "Root entry read error\n");
					return -1;
		}
	}
	return 0;
}

int init_fat(int fresh) {
	// if an index is pointing to itself, then EOF
	// if an index is pointing to some other index, then its a link
	// if an index is pointing to -1, then block is empty
	if (fresh) {
		int i;

		for (i = 0; i < CAPACITY; i++) {
			fat_entries[i].next = -1;
		}
		if (write_blocks(1 + super.root_blocks, super.fat_blocks, fat_entries) != super.fat_blocks) {
			fprintf(stderr, "FAT entry init error\n");
			return -1;
		}

	} else {
		if (read_blocks(1 + super.root_blocks, super.fat_blocks, fat_entries) != super.fat_blocks) {
			fprintf(stderr, "FAT entry read error\n");
			return -1;
		}
	}
	return 0;
}


void init_fd_table() {
	// this table only exists in the memory
	int i;

	for (i = 0; i < MAX_FILES; i++) {
		fd_table[i].directory_idx = -1;
		fd_table[i].read_ptr = -1;
		fd_table[i].write_ptr = -1;
	}
}

int free_fd_spot() {
	int i;
	for (i = 0; i < MAX_FILES; i++) {
		if (fd_table[i].directory_idx == -1) {
			return i;
		}
	}
	fprintf(stderr, "FD table full\n");
	return -1;
}

int free_root_spot() {
	int i;
	for (i = 0; i < MAX_FILES; i++) {
		if (root_entries[i].file_name[0] == '\0') {
			return i;
		}
	}
	fprintf(stderr, "Root table full\n");
	return -1;
}

int next_free_block() {
	int i;
	for (i = 0; i < CAPACITY; i++) {
		if (fat_entries[i].next == -1) {
			return i;
		}
	}
	fprintf(stderr, "Out of space\n");
	return -1;
}

int sfs_fcreate(char* name) {
	int root_index, fd_index;
	// find the first available entry on Root
	if ((root_index = free_root_spot()) == -1)
		return -1;
	if (strlen(name) > FILE_NAME_LENGTH) {
		fprintf(stderr, "File name length error\n");
		return -1;
	}

	strncpy((char*)&root_entries[root_index].file_name[0], name, FILE_NAME_LENGTH);
	root_entries[root_index].attributes.size = 0;
	get_date(root_entries[root_index].attributes.modified);

	if ((fd_index = free_fd_spot()) == -1)
		return -1;

	root_disk_update(root_index);

	//update the FD table in memory
	fd_table[fd_index].directory_idx = root_index;
	fd_table[fd_index].read_ptr = 0;
	fd_table[fd_index].write_ptr = 0;
	return fd_index;
}

int super_disk_update() {
	if (write_blocks(0, 1, &super) != 1) {
		fprintf(stderr, "Super block create error\n");
		return 0;
	} else {
		return -1;
	}
}

int fat_disk_update(int fat_index) {
	short block_num, fat_entries_in_block;
	fat_entries_in_block = BLOCK_SIZE/sizeof(Fat_entry);
	//find which block was updated
	block_num = fat_index / fat_entries_in_block;
	if (write_blocks(1+super.root_blocks+block_num, 1,
			&fat_entries[block_num * fat_entries_in_block]) != 1) {
		fprintf(stdout, "FAT disk update error\n");
		return -1;
		}
	return 0;
}

int root_disk_update(int root_index) {
	short block_num, num_roots_in_block;
	num_roots_in_block = BLOCK_SIZE/sizeof(Folder_entry);
	// find which block was updated.
	block_num = root_index / super.root_blocks;
	if (write_blocks(1+block_num, 1, &root_entries[block_num * num_roots_in_block]) != 1) {
		fprintf(stdout, "Root disk update error\n");
		return -1;
	}
	return 0;
}

int data_block_read(int block_num, char* buf) {
	if (read_blocks(1 + super.fat_blocks + super.root_blocks + block_num, 1,
			buf) != 1) {
		fprintf(stderr, "Unable to read data block\n");
		return -1;
	}
	return 0;
}

int data_block_update(int block_num, char* buf) {
	if (write_blocks(1 + super.fat_blocks + super.root_blocks + block_num, 1,
			buf) != 1) {
		fprintf(stderr, "Unable to write data block\n");
		return -1;
	}
	return 0;
}

int fd_file_status(int fileID) {
	// check if the file ID is valid
	if (fileID < 0 || fileID > MAX_FILES) {
		fprintf(stderr, "Invalid file ID\n");
		return -1;
	}
	// check if the file is even open
	if (fd_table[fileID].directory_idx == -1) {
		fprintf(stderr, "File not open\n");
		return -1;
	}
	return 0;
}

/*************************************************************
 * Public functions
 ************************************************************/
int mksfs(int fresh) {

	if (fresh) {
		// fresh flag true so initialize a new disk
		if (init_fresh_disk(disk_name, BLOCK_SIZE, CAPACITY) != 0)
			return -1;
	} else {
		if (init_disk(disk_name, BLOCK_SIZE, CAPACITY) != 0)
			return -1;
	}

	if (init_super_block(fresh) != 0) {
		return -1;
	}
	if (init_root(fresh) != 0) {
		return -1;
	}
	if (init_fat(fresh) != 0) {
		return -1;
	}
	init_fd_table();

	return 0;
}

void sfs_ls(void) {
	int i, j, block_cnt;
	Fat_entry* fat_entry;
	printf("File Name\t\tModified\t\tSize\t\tBlocks\n");

	for (i = 0; i < MAX_FILES; i++) {
		if (root_entries[i].file_name[0] != '\0') {
			printf("%s\t\t", root_entries[i].file_name);
			printf("%s\t\t", root_entries[i].attributes.modified);
			printf("%d\t\t", root_entries[i].attributes.size);

			block_cnt = root_entries[i].attributes.size / BLOCK_SIZE;
			if (root_entries[i].attributes.size % BLOCK_SIZE > 0)
				block_cnt++;

			j = 0;
			if (block_cnt > 0) {
				printf("%d", root_entries[i].head_idx);
				fat_entry = &fat_entries[root_entries[i].head_idx];
			}
			while (j < block_cnt) {
				printf(",%d", fat_entry->next);
				fat_entry = &fat_entries[fat_entry->next];
				j++;
			}
			printf("\n");
		}
	}
}

int sfs_fopen(char *name) {
	int i, root_index, fd_index;
	root_index = -1;;
	// check if the file exists in the directory
	for (i = 0; i < MAX_FILES; i++) {
		if (strcmp(name, root_entries[i].file_name) == 0) {
			root_index = i;
			break;
		}
	}

	if (root_index != -1) {
		// if exists, check if the file is already open
		for (i = 0; i < MAX_FILES; i++) {
			if (root_index == fd_table[i].directory_idx) {
				fprintf(stderr,  "File already open\n");
				return -1;
			}
		}

		fd_index = free_fd_spot();
		if (fd_index == -1)
			return -1;
		fd_table[fd_index].directory_idx = root_index;
		fd_table[fd_index].read_ptr = 0;
		fd_table[fd_index].write_ptr = root_entries[root_index].attributes.size;
		return fd_index;
	} else {
		return sfs_fcreate(name);
	}

}

int sfs_fclose(int fileID) {
	// check validity of fileID in FD table
	if (fd_file_status(fileID) == -1)
		return -1;

	fd_table[fileID].directory_idx = -1;
	fd_table[fileID].read_ptr = -1;
	fd_table[fileID].write_ptr = -1;
	return 0;

}

int sfs_fwrite(int fileID, char *buf, int length) {
	Folder_entry* root_entry;
	Fd_entry* fd_entry;
	Fat_entry* fat_entry;
	int block_cnt, ptr_block, bytes_rem, curr_block, bytes_to_write, i, total_bytes, next_block;
	char* temp;

	total_bytes = 0;
	// check validity of fileID in FD table
	if (fd_file_status(fileID) == -1)
		return -1;

	temp = (char*) malloc(BLOCK_SIZE);

	fd_entry = &fd_table[fileID];
	root_entry = &root_entries[fd_entry->directory_idx];

	// number of blocks currently occupied by the file
	block_cnt = root_entry->attributes.size / BLOCK_SIZE;
	if (root_entry->attributes.size % BLOCK_SIZE > 0)
		block_cnt++;

	// check if some blocks need to be overwritten or some space is left in a block
	if (block_cnt > 0 && fd_entry->write_ptr < (BLOCK_SIZE * block_cnt)) {
		// block number for current position of write ptr
		ptr_block = fd_entry->write_ptr / BLOCK_SIZE;

		fat_entry = &fat_entries[root_entry->head_idx];
		curr_block = root_entry->head_idx;

		i = 0;
		while (i < ptr_block) {
			curr_block = fat_entry->next;
			fat_entry = &fat_entries[curr_block];
			i++;
		}
		// bytes remaining in current block
		bytes_rem = BLOCK_SIZE - (fd_entry->write_ptr % BLOCK_SIZE);
		// overwrite the current blocks
		while ((ptr_block < block_cnt) && (length > 0)) {
			bytes_to_write = MIN(length, bytes_rem);
			if (bytes_to_write > 0 && bytes_to_write < BLOCK_SIZE) {
				// read the block to overwrite some bytes
				if (data_block_read(curr_block, temp) != 0)
					return -1;
			}
			memcpy(temp + BLOCK_SIZE - bytes_rem, buf, bytes_to_write);
			//update block on disk
			if (data_block_update(curr_block, temp) != 0)
				return -1;
			length -= bytes_to_write;
			ptr_block++;
			buf += bytes_to_write;
			fd_entry->write_ptr += bytes_to_write;
			root_entry->attributes.size += bytes_to_write;
			total_bytes += bytes_to_write;
			if (ptr_block < block_cnt) {
				curr_block = fat_entry->next;
				fat_entry = &fat_entries[curr_block];
				bytes_rem = BLOCK_SIZE;
			}
		}
	}

	//all available blocks overwritten. Check if more blocks are required.

	if (length > 0) {

		block_cnt = length / BLOCK_SIZE;
		if (length % BLOCK_SIZE > 0)
			block_cnt++;

		i = 0;

		while (i < block_cnt) {
			// allocate block
			if ((next_block = next_free_block()) == -1)
				return -1;
			if (root_entry->attributes.size == 0) {
				root_entry->head_idx = next_block;
				fat_entry = &fat_entries[next_block];
			}
			curr_block = next_block;
			fat_entry->next = next_block;
			fat_entry = &fat_entries[curr_block];
			fat_entry->next = next_block;
			// calculate the number of bytes to write
			bytes_to_write = MIN(length, BLOCK_SIZE);
			memcpy(temp, buf, bytes_to_write);
			//update block on disk
			if (data_block_update(curr_block, temp) != 0)
				return -1;
			//update fat table
			fat_disk_update(curr_block);
			root_entry->attributes.size += bytes_to_write;
			total_bytes += bytes_to_write;
			length -= bytes_to_write;
			buf += bytes_to_write;
			fd_entry->write_ptr += bytes_to_write;
			i++;
		}
		super.free_blocks -= block_cnt;
		super_disk_update();
		root_disk_update(fd_entry->directory_idx);
	}

	free(temp);

	return total_bytes;
}

int sfs_fread(int fileID, char *buf, int length) {

	// check validity of fileID in FD table
		if (fd_file_status(fileID) == -1)
			return -1;

		Fd_entry* fd_entry;
		Folder_entry* root_entry;
		Fat_entry* fat_entry;
		int block_num, curr_block, i, start_point, bytes_to_read, total_bytes;
		char* temp;


		temp = malloc(BLOCK_SIZE);

		fd_entry = &fd_table[fileID];
		root_entry = &root_entries[fd_entry->directory_idx];
		total_bytes = 0;
		// check if the length is out of bounds
		//if (length + fd_entry->read_ptr > root_entry->attributes.size) {
		//	fprintf(stderr, "Out of bound read error\n");
		//	return -1;
		//}
		length = MIN(length, root_entry->attributes.size - fd_entry->read_ptr);

		curr_block = root_entry->head_idx;
		fat_entry = &fat_entries[root_entry->head_idx];
		block_num = fd_entry->read_ptr / BLOCK_SIZE;
		i = 0;
		//determine the block to start reading from
		while (i < block_num) {
			curr_block = fat_entry->next;
			fat_entry = &fat_entries[curr_block];
			i++;
		}

		//read the first block which may not start from index 0.
		start_point = fd_entry->read_ptr % BLOCK_SIZE;
		bytes_to_read = MIN(length, BLOCK_SIZE - start_point);
		if (data_block_read(curr_block, temp) != 0)
			return -1;
		memcpy(buf, temp + start_point, bytes_to_read);
		buf += bytes_to_read;
		length -= bytes_to_read;
		fd_entry->read_ptr += bytes_to_read;
		total_bytes += bytes_to_read;

		while (length > 0) {
			curr_block = fat_entry->next;
			fat_entry = &fat_entries[curr_block];

			bytes_to_read = MIN(length, BLOCK_SIZE);
			if (data_block_read(curr_block, temp) != 0)
				return -1;
			memcpy(buf, temp, bytes_to_read);
			buf += bytes_to_read;
			length -= bytes_to_read;
			total_bytes += bytes_to_read;
			fd_entry->read_ptr += bytes_to_read;
		}

		free(temp);
		return total_bytes;

}

int sfs_fseek(int fileID, int offset) {
	// check validity of fileID in FD table
	if (fd_file_status(fileID) == -1)
		return -1;

	Folder_entry* root_entry;
	root_entry = &root_entries[fd_table[fileID].directory_idx];
	//check if the offset is out of bound
	if (offset < 0 || offset > root_entry->attributes.size) {
		fprintf(stderr, "Pointer out of bound\n");
		return -1;
	}
	// update fd table
	fd_table[fileID].read_ptr = offset;
	fd_table[fileID].write_ptr = offset;

	return 0;
}

int sfs_remove(char *file) {
	int root_index, i, block_cnt,curr_fat_index, next_fat_index;
	Folder_entry* root_entry;
	Fat_entry *curr_fat;

	root_index = -1;;
	// check if the file exists in the directory
	for (i = 0; i < MAX_FILES; i++) {
		if (strcmp(file, root_entries[i].file_name) == 0) {
			root_index = i;
			break;
		}
	}
	if (root_index == -1) {
		fprintf(stderr, "File not found!\n");
		return -1;
	}

	// if file is found, free blocks on FAT table, super block update root table, and fd table
	root_entry = &root_entries[root_index];

	// fd table
	// find the entry and make it free.
	for (i = 0; i < MAX_FILES; i++) {
		if (root_index == fd_table[i].directory_idx) {
			fd_table[i].directory_idx = -1;
			fd_table[i].read_ptr = -1;
			fd_table[i].write_ptr = -1;
			break;
		}
	}

	curr_fat_index = root_entry->head_idx;
	curr_fat = &fat_entries[root_entry->head_idx];
	next_fat_index = curr_fat->next;
	// Fat table
	while(next_fat_index != -1) {
		curr_fat->next = -1;
		fat_disk_update(curr_fat_index);
		curr_fat = &fat_entries[next_fat_index];
		curr_fat_index = next_fat_index;
		next_fat_index = curr_fat->next;
	}

	// super block
	block_cnt = root_entry->attributes.size / BLOCK_SIZE;
		if (root_entry->attributes.size % BLOCK_SIZE > 0)
		block_cnt++;

	super.free_blocks += block_cnt;

	// root directory
	root_entry->head_idx = -1;
	root_entry->attributes.size = -1;
	i = 0;
	for (i = 0; i < FILE_NAME_LENGTH; i++) {
		root_entry->file_name[i] = '\0';
		root_entry->attributes.modified[i] = '\0';
	}

	//update disk
	super_disk_update();
	root_disk_update(root_index);

	return 0;
}

/*************************************************************
 * Unit tests
 ************************************************************/

//void test1() {
//	char modified[30];
//	get_date(modified);
//
//	printf("%s\n", modified);
//
//}
//
//int main() {
//	test1();
//	//printf("jdslf");
//	return 0;
//}
