Student Name: Abdul Haseeb Jehangir
Student ID:   260399461

The assignment is complete. It compiled and ran OK on ubuntu 14.04 LTS on a
personal PC. I dont see any reason why it will not run on Trottier 3rd floor.
It provides the following API:


/****************************************************************************
  API
 ***************************************************************************/
int mksfs(int fresh);
void sfs_ls(void);
int sfs_fopen(char *name);
int sfs_fclose(int fileID);
int sfs_fwrite(int fileID, char *buf, int length);
int sfs_fread(int fileID, char *buf, int length);
int sfs_fseek(int fileID, int offset);
int sfs_remove(char *file);

/****************************************************************************
  COMPILE INSTRUCTIONS
 ***************************************************************************/
Source code can be compiled with one of the two test files in src/
Following commands can be used to compile with sfs_ftest.c and sfs_htest.c
respectively.

>> cd ./src/
>> gcc disk_emu.c sfs_api.c sfs_ftest.c -I ../inc/ -o assignment4
OR
>> gcc disk_emu.c sfs_api.c sfs_htest.c -I ../inc/ -o assignment4

/****************************************************************************
  EXECUTION INSTRUCTIONS
 ***************************************************************************/
>> ./assignment4

/****************************************************************************
  NOTES and LIMITATIONS
 ***************************************************************************/
 - BLOCK SIZE = 1024 bytes
 - DISK CAPACity = 16384 BLOCKS ~ 16 MB
 - MAX FILES = 256
 - These values can be changed by the user. Assuming the values are correct,
   the funtionality of the API should remain consistent.
 - To save time, I used array for all caching and did not attempt to use
   dynamic memory structures.
 - FAT, ROOT table, SUPER BLOCK are cached in the memory.
 - I did not use any free block list. FAT table serves all purpose for this
   assignment.
 - For data block allocation, I use first available block starting from index 0.
   I understand this can cause alot of file fragmentation. However, I hope that
   functionality is what counts here more. Assignment specification did not
   specify any particular allocation approach.
 - Total time spent on assignment including debugging and testing ~ 19 hrs.
 - I apologize in advance, the code is not well commented. I understand this is
   a very bad practice. I have a job interview in 2 days and I hope that this is
   an acceptable excuse.
 
 /****************************************************************************
  TESTING AND VERIFICATION
 ***************************************************************************/
 The source code was tested with two provided test files and noted in section
 above (COMPILE INSTRUCTIONS).
 The code runs successfully with both test file WITHOUT ANY ERRORS. Therefore,
 functionally, the API implementation is 100% correct.
 HOWEVER, in some runs, the code crashes with segment fault with test file
 sfs_htest.c. This occurs when the test tries to fill up the disk and cannot
 write anymore. I tried to debug this issue, but could not find the problem.
 Also this seg falut is hard to catch since its rare.
 
