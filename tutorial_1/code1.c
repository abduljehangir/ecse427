#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#define MAX_COUNT 200
#define BUF_SIZE 100

void child_process(void);
void parent_process(void);

trial_exec()
{
  // What exec does is, if you pick any process and pass it to exec
  // it will execute.
  // You're running P, you call fork and in the child part you want
  // to execute a different code, you call the exec function with a
  // path (that needs to be executed)
  // fork returns 0 to the current process.
	// execl(char *path, char *arg, ...);
	// execv(char *path, char *argv[]);
	// execle(char *path, char *arg, ..., char *envp[]);
	// execlp();
	// execvp(char *file, char *argv[]);
	// l means argument list
	// v means argument vector
	// c means environment vector
	// p means search a path
	pid_t pid;
  // fork return 0 to the child process and returns pid of child
  // process to the parent
  pid = fork();
  if (pid == 0) {
    child_process();
	}
  else {
    parent_process();
  }
}

trial_wait()
{
  // pid_t wait()
  // parent waits for all the child processes/pid to complete
  //       wait_pid()
  // parent waits for completions of a particular pid
}

void child_process()
{
  int i;
  for (i = 1; i < MAX_COUNT; i++) {
    printf("I'm child\n");
  }
}

void parent_process()
{
  int i;
  for (i = 1; i < MAX_COUNT; i++) {
    printf("I'm parent\n");
  }
}
trial_fork()
{
  pid_t pid;
  int i;
  char buf[BUF_SIZE];
  fork();
	// getpid() returns the process id
  pid = getpid();
  for(i = 1; i < MAX_COUNT; i++) {
    printf("this line is from Pid %d value %d\n", pid, i);
  }
}

void main()
{
  trial_exec();                     
}
